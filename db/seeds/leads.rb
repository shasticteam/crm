require 'fastercsv'

csv_file = 'db/seeds/crm-leads.csv'
data = FasterCSV.read(csv_file, :headers => true) # data = FasterCSV.read(csv_file, :headers => true, :header_converters => :symbol)

puts "	Importing Leads ====================================="

@imported = 0
@skipped = 0

@admin = User.find_by_email('joseariel@shastic.com')

data.each do |ld|
	[ 'Marketing Manager', 'CEO', 'COO', 'Head of RE Lending', 'eCommerce Manager', 'Loan Manager', 'Executive Assistant to CEO' ].each do |title|
		
		@first_name = ld[title]
		
		unless ( @first_name.blank? || ld['Company'].blank? || Lead.exists?(:first_name => @first_name, :company => ld['Company']) )
			lead = Lead.new({ :user_id => @admin.id,
												:company => ld['Company'], 
												:phone => ld['phone'], 
												:blog => ld['blog'], 
												:first_name => @first_name, 
												:background_info => ld['background_info'], 
												:business_address_attributes => { :street1 => ld['street1'], 
																													:city => ld['City'], 
																													:state => ld['State'], 
																													:zipcode => ld['zipcode'], 
																													:address_type => 'Business' } }) 
																															
			puts "		Lead: #{lead.first_name} at #{lead.company} => #{lead.save.inspect}"
			@imported += 1
		else
			@skipped += 1
			puts "		Skipped: #{@first_name || title} at #{ld['Company']}"
		end
	end

end

puts "	#{@imported} Leads Imported, #{@skipped} Skipped"
puts "	====================================="

# Users

[
	{ :username => "lou", :password => "shastic123", :email => "david@shastic.com" },
	{ :username => "intern", :password => "shastic123", :email => "intern@shastic.com" }
].each do |attrs|
	user = User.find_by_username(attrs[:username]) || User.new
  user.update_attributes(:username => attrs[:username], :password => attrs[:password], :email => attrs[:email])
  user.update_attribute(:admin, true) # Mass assignments don't work for :admin because of the attr_protected
  user.update_attribute(:suspended_at, nil) # Mass assignments don't work for :suspended_at because of the attr_protected
end